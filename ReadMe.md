# BlackSwanTest
_Created By Balázs Tóth_


### Open source libraries

The project dependencies are handled with Cocoapods, and the libraries included are the following:

- **Alamofire** for networking
- **JSONCodable** for easier JSON converting

I also reused a network reachability monitoring class from one of my previous projects.

### About the application

This application is a very simple client for viewing the popular site [www.reddit.com](www.reddit.com) and some of its many subreddits.

The app has two views, both contain a UITableView to show contents in a structured manner. The subreddit chooser view is a UITableViewController with basic cells. This view also reacts to changes to the users network reachability.

You can add and delete subreddits as you choose, but when the application starts if there are no saved subreddits, some are suggested for you.

Selecting any of the subreddits opens up their first page, and shows some basic information about each post:
  * The title of the post
  * An image if it has at least one
  * The name of the author
  * The score it currently has

Pressing the refresh button reloads the posts. All images are lazy loaded, and there is a small image cache for handling subsequent loads of the same image.

I tried to employ a few different techniques and methodologies (even for the same problem) while creating this application, to hopefully better showcase my skills and understanding of iOS fundamentals.
