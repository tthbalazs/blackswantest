//
//  RedditAPIClient.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 01/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import Alamofire

typealias JSONDictionary = [String: AnyObject]

final class RedditAPIClient {
    static let sharedClient = RedditAPIClient()
    
    let baseURL = "https://www.reddit.com"
    let jsonEnding = ".json"
    
    func requestSubRedditContent(subReddit: String, callback: ([Post], NSError?) -> Void) {
        let url = baseURL + subReddit + jsonEnding
        
        Alamofire.request(.GET, url).responseJSON { (result) in
            var posts: [Post] = []
            
            guard let respJSON = result.result.value as? JSONDictionary where result.result.error == nil else {
                callback(posts, result.result.error)
                
                return
            }
            
            /*
             * Each subreddit has its posts inside a dictionary called "children". 
             * Each children has a data property that contains all the necessary details.
             */
            
            if let data = respJSON["data"] as? JSONDictionary {
                if let children = data["children"] as? [JSONDictionary] {
                    for child in children {
                        guard let childData = child["data"] as? JSONDictionary else {
                            continue
                        }
                        
                        let post: Post?
                        
                        do {
                            post = try Post(object: childData)
                        } catch _ {
                            post = nil
                        }
                        
                        if let post = post {
                            
                            // Just to be safe for work...
                            
                            if !post.over_18 {
                                posts.append(post)
                            }
                        }
                    }
                }
            }
            
            callback(posts, nil)
        }
    }
}