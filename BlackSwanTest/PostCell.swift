//
//  PostCell.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import UIKit

final class PostCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView?
    
    let placeholder = UIImage(named: "Placeholder")!
    
    func configureForPost(post: Post) {
        titleLabel.text = post.title
        authorLabel.text = post.author
        scoreLabel.text = "\(post.score)"
        
        guard let images = post.images, postImageView = postImageView else {
            return
        }
        
        let screenWidth = UIScreen.mainScreen().bounds.width
        var bestImageForScreenWidth: PostImage? = nil
        
        for image in images {
            if let bestImage = bestImageForScreenWidth {
                if abs(screenWidth - CGFloat(image.width)) < abs(screenWidth - CGFloat(bestImage.width)) {
                    bestImageForScreenWidth = image
                }
            } else {
                bestImageForScreenWidth = image
            }
        }
        
        guard let bestImage = bestImageForScreenWidth, url = bestImage.imageURL else {
            return
        }
        
        postImageView.setImageWithUrl(url, placeHolderImage: placeholder)
    }
}