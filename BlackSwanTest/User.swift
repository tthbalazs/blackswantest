//
//  User.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import Foundation

final class User {
    static let currentUser = User()
    
    let subRedditsKey = "SavedSubreddits"
    
    var subReddits: [SubReddit]
    
    init() {
        subReddits = []
        
        loadSubReddits()
    }
    
    // MARK: - Add a new subreddit
    
    func addSubReddit(name: String) {
        subReddits.append(SubReddit(name: name))
        
        saveSubReddits()
    }
    
    func removeSubReddit(name: String) {
        let idx = subReddits.indexOf { (sr) -> Bool in
            if let itemName = sr.name where itemName == name {
                return true
            } else {
                return false
            }
        }
        
        if idx != nil {
            subReddits.removeAtIndex(idx!)
            
            saveSubReddits()
        }
    }
    
    func removeSubReddit(subreddit: SubReddit) {
        if let name = subreddit.name {
            removeSubReddit(name)
        }
    }
    
    // MARK: - Load & Save user subreddits
    
    func loadSubReddits() {
        if let subRedditArray = NSUserDefaults.standardUserDefaults().valueForKey(subRedditsKey) as? [[String: AnyObject]] where subRedditArray.count > 0 {
            for subRedditDictionary in subRedditArray {
                subReddits.append(SubReddit(dictionary: subRedditDictionary))
            }
        } else {
            // If the user has no subreddits, lets provide some as samples
            
            let sampleSubReddits = [
                "/r/all",
                "/r/funny",
                "/r/food",
                "/r/pics",
                "/r/aww",
                "/r/mildlyinteresting",
                "/r/thissubredditdoesnotexist"
            ]
            
            for sample in sampleSubReddits {
                subReddits.append(SubReddit(name: sample))
            }
        }
    }
    
    func saveSubReddits() {
        var subRedditArray: [[String: AnyObject]] = []
        
        for subReddit in subReddits {
            if let dict = subReddit.encode() {
                subRedditArray.append(dict)
            }
        }
        
        NSUserDefaults.standardUserDefaults().setValue(subRedditArray, forKey: subRedditsKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}