//
//  SubReddit.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import Foundation

struct SubReddit {
    let name: String?
    
    init(name: String) {
        self.name = name
    }
    
    // MARK: - Encode & Decode
    
    init(dictionary: [String: AnyObject]) {
        name = dictionary["name"] as? String
    }
    
    func encode() -> [String: AnyObject]? {
        guard let name = self.name else {
            return nil
        }
        
        return ["name": name]
    }
}