//
//  LoadingCell.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import UIKit

final class LoadingCell: UITableViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func animate() {
        if !activityIndicator.isAnimating() {
            activityIndicator.startAnimating()
        }
    }
}