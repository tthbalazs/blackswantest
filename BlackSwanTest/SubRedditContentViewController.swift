//
//  SubRedditContentViewController.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 01/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import UIKit

enum CellIdentifiers: String {
    case NoContent = "NoContentCell"
    case Loading = "LoadingCell"
    case PostCellWithImage = "PostCellWithImage"
    case PostCellWithOutImage = "PostCellWithOutImage"
}

final class SubRedditContentViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var selectedSubreddit: SubReddit?
    var posts: [Post] = []
    
    var noContent = false
    var loading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let subReddit = selectedSubreddit {
            navigationItem.title = subReddit.name
        }
        
        tableView.estimatedRowHeight = 544
        tableView.rowHeight = UITableViewAutomaticDimension
        
        loadSubRedditData()
    }
    
    // MARK: - Data Handling
    
    func loadSubRedditData() {
        guard let subReddit = selectedSubreddit?.name else {
            return
        }
        
        loading = true
        
        tableView.reloadData()
        
        RedditAPIClient.sharedClient.requestSubRedditContent(subReddit) { (posts, error) in
            if error != nil || posts.count == 0 {
                self.noContent = true
            }
            
            self.loading = false
            self.posts = posts
            
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Actions
    
    @IBAction func reloadData() {
        posts = []
        
        loadSubRedditData()
    }
}

// MARK: - UITableViewDataSource

extension SubRedditContentViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if noContent || loading {
            return 1
        }
        
        return posts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if noContent {
            let noContentCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.NoContent.rawValue)!
            
            return noContentCell
        }
        
        if loading {
            let loadingCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.Loading.rawValue) as! LoadingCell
            
            loadingCell.animate()
            
            return loadingCell
        }
        
        let post = posts[indexPath.row]
        var postCell: PostCell? = nil
        
        if post.hasImages {
            postCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.PostCellWithImage.rawValue) as? PostCell
        } else {
            postCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.PostCellWithOutImage.rawValue) as? PostCell
        }
        
        postCell!.configureForPost(post)
        
        return postCell!
    }
}