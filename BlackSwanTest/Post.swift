//
//  Post.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 01/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import JSONCodable

struct Post {
    let author: String?
    let name: String?
    let over_18: Bool
    let score: Int
    let title: String?
    let visited: Bool
    
    var images: [PostImage]?
    
    var hasImages: Bool {
        if let images = images where images.count > 0 {
            return true
        } else {
            return false
        }
    }
}

struct PostImage {
    let height: Float
    let width: Float
    let url: String
    
    var imageURL: NSURL? {
        // This is a little bit costly, but the URLs for the scaled images are HTML encoded, so we have to decode them
        let decodedString = String(htmlEncodedString: url)
        
        if let url = NSURL(string: decodedString) {
            return url
        } else {
            return nil
        }
    }
}

extension PostImage: JSONDecodable {
    init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        
        height = try decoder.decode("height")
        width = try decoder.decode("width")
        url = try decoder.decode("url")
    }
}

extension Post: JSONDecodable {
    init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        
        author = try decoder.decode("author")
        name = try decoder.decode("name")
        over_18 = try decoder.decode("over_18")
        score = try decoder.decode("score")
        title = try decoder.decode("title")
        visited = try decoder.decode("visited")
        
        // The JSONCodable library is good for quickly parsing simple objects, but it has a problem with deeply nested json dictionaries. The solution below is not optimal, but a simple workaround for the problem
        
        if let preview = object["preview"] as? JSONDictionary, postImages = preview["images"] as? [JSONDictionary] {
            
            
            guard let firstImage = postImages.first else {
                images = nil
                
                return
            }
            
            images = []
            
            if let resolutions = firstImage["resolutions"] as? [JSONObject] {
                for image in resolutions {
                    let postImage: PostImage?
                    
                    do {
                        postImage = try PostImage(object: image)
                    } catch _ {
                        postImage = nil
                    }
                    
                    if let postImage = postImage {
                        images!.append(postImage)
                    }
                }
            }
        }
    }
}