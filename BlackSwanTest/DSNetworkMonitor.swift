//
//  DSNetworkMonitor.swift
//
//  Created by Tóth Balázs on 20/05/15.
//  Copyright (c) 2015 Delight Solutions. All rights reserved.
//

import Foundation
import SystemConfiguration

public final class DSNetworkMonitor {
    
    static let sharedInstance = DSNetworkMonitor()
    static let reachabilityNotification = "connectionStatusChanged"
    
    private var dispatch_timer: dispatch_source_t?
    private lazy var timer_queue: dispatch_queue_t = {
        return dispatch_queue_create("com.tthbalazs.BlackSwanTest-reachability", nil)
    }()
    
    var previousStatus: Bool = false
    var currentStatus: Bool = false

    init() {
        self.startMonitoringReachability()
    }

    // MARK: - Reachability
    
    func startMonitoringReachability() {
        if let timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, timer_queue) {
            dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, 0), 1000 * NSEC_PER_MSEC, 1000 * NSEC_PER_MSEC)
            dispatch_source_set_event_handler(timer, { [unowned self] in
                if self.statusChanged() {
                    NSNotificationCenter.defaultCenter().postNotificationName(DSNetworkMonitor.reachabilityNotification, object: nil)
                }
            })
            
            self.dispatch_timer = timer
            dispatch_resume(timer)
        }
    }
    
    func statusChanged() -> Bool {
        self.currentStatus = self.hasNetworkConnection()
        
        if self.previousStatus != self.currentStatus {
            self.previousStatus = self.currentStatus
            
            return true
        } else {
            return false
        }
    }
    
    func hasNetworkConnection() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection) ? true : false
    }
}