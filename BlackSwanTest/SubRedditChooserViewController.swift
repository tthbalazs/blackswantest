//
//  SubRedditChooserViewController.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 01/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import UIKit

final class SubRedditChooserViewController: UITableViewController {
    let subRedditCellIdentifier = "SubRedditCell"
    let showSubRedditContentSegueIdentifier = "ShowSubredditContent"
    
    var selectedSubreddit: SubReddit? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkConnectionStatus()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SubRedditChooserViewController.checkConnectionStatus), name: DSNetworkMonitor.reachabilityNotification, object: nil)
    }
    
    // MARK: - Connection status change
    
    func checkConnectionStatus() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            if DSNetworkMonitor.sharedInstance.hasNetworkConnection() {
                self.navigationItem.title = "Choose a subreddit"
            } else {
                self.navigationItem.title = "No internet connection :("
            }
        })       
    }
    
    // MARK: - TableViewDataSource methods
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User.currentUser.subReddits.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(subRedditCellIdentifier)!
        let subReddit = User.currentUser.subReddits[indexPath.row]
        
        cell.textLabel?.text = subReddit.name
        
        return cell
    }
    
    // MARK: - UITableViewDelegate methods
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        selectedSubreddit = User.currentUser.subReddits[indexPath.row]
        
        performSegueWithIdentifier(showSubRedditContentSegueIdentifier, sender: nil)
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle{
        case .Delete:
            let subReddit = User.currentUser.subReddits[indexPath.row]
            
            User.currentUser.removeSubReddit(subReddit)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
            
            break
        default:
            break
        }
    }
    
    // MARK: - Segue from cell selection
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == showSubRedditContentSegueIdentifier {
            if segue.destinationViewController is SubRedditContentViewController {
                (segue.destinationViewController as! SubRedditContentViewController).selectedSubreddit = selectedSubreddit
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func addSubReddit() {
        let alertController = UIAlertController(title: "Add a new subreddit", message: nil, preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        let addAction = UIAlertAction(title: "Add", style: .Default) { (_) in
            let subRedditTextField = alertController.textFields![0] as UITextField
            
            if let name = subRedditTextField.text {
                User.currentUser.addSubReddit(name)
                
                let indexPaths = [NSIndexPath(forRow: User.currentUser.subReddits.count - 1, inSection: 0)]
                
                self.tableView.beginUpdates()
                self.tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
                self.tableView.endUpdates()
            }
        }
        addAction.enabled = false
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "/r/all"
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                addAction.enabled = textField.text != ""
            }
        }
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func toggleEditing(sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.editing, animated: true)
        
        if tableView.editing {
            sender.title = "Save"
        } else {
            sender.title = "Edit"
            
            // Save changes
            
            User.currentUser.saveSubReddits()
        }
    }
}