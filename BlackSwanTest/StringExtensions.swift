//
//  StringExtensions.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

extension String {
    init(htmlEncodedString: String) {
        do {
            let encodedData = htmlEncodedString.dataUsingEncoding(NSUTF8StringEncoding)!
            let attributedOptions : [String: AnyObject] = [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
            ]
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            
            self.init(attributedString.string)
        } catch {
            fatalError("Cannot convert html string: \(error)")
        }
    }
}