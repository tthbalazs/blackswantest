//
//  BlackSwanTestUITests.swift
//  BlackSwanTestUITests
//
//  Created by Tóth Balázs on 01/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import XCTest

class BlackSwanTestUITests: XCTestCase {
    
    let timeout: NSTimeInterval = 5
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
    }
    
    // MARK: - SubRedditContentView tests
    
    func testSubRedditContent() {
        XCUIDevice.sharedDevice().orientation = .Portrait
        
        let app = XCUIApplication()
        let exists = NSPredicate(format: "exists == 1")
        
        app.tables.staticTexts["/r/all"].tap()
        
        XCTAssert(app.navigationBars["/r/all"].exists, "The subreddit name should be present on the NavigationBar")
        
        expectationForPredicate(exists, evaluatedWithObject: app.tables.element.cells.elementBoundByIndex(1), handler: nil)
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertGreaterThan(app.tables.element.cells.count, 1, "There should more than one cells in the tableView")
    }
    
    func testEmptySubRedditContent() {
        XCUIDevice.sharedDevice().orientation = .Portrait
        
        let app = XCUIApplication()
        let exists = NSPredicate(format: "exists == 1")
        
        app.tables.staticTexts["/r/thissubredditdoesnotexist"].tap()
        
        expectationForPredicate(exists, evaluatedWithObject: app.tables.element.cells.elementBoundByIndex(0).staticTexts["It looks like there is nothing to see here..."], handler: nil)
        waitForExpectationsWithTimeout(timeout, handler: nil)
        
        XCTAssertEqual(app.tables.element.cells.count, 1, "There should be only one cell in the tableView")
    }
}
