//
//  PostTests.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import XCTest
@testable import BlackSwanTest

class PostTests: XCTestCase {
    var testPost: Post!
    var testPostWithImage: Post!
    var invalidPostImage: PostImage!
    
    override func setUp() {
        super.setUp()
        
        testPost = Post(
            author: "Test Author",
            name: "Some name",
            over_18: false,
            score: 1000,
            title: "Title of the post",
            visited: false,
            images: nil
        )
        
        testPostWithImage = Post(
            author: "Test Author",
            name: "Some name",
            over_18: false,
            score: 1000,
            title: "Title of the post",
            visited: false,
            images: [
                PostImage(
                    height: 200, 
                    width: 100,
                    url: "http://www.google.com"
                )
            ]
        )
        
        invalidPostImage = PostImage(
            height: 200,
            width: 100,
            url: "ht*!\\p:/ww.google.com"
        )
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Post tests
    
    func testPostImages() {
        XCTAssertTrue(testPostWithImage.hasImages, "A post that has images should return true on the hasImages property")
        XCTAssertFalse(testPost.hasImages, "A post that has no images should return false on the hasImages property")
    }
    
    func testPostImageURL() {
        for image in testPostWithImage.images! {
            XCTAssertNotNil(image.imageURL, "A post image should have a valid URL")
        }
    }
    
    func testPostImageMalformedURL() {
        XCTAssertNil(invalidPostImage.imageURL, "A malformed url should not be returned")
    }
}
