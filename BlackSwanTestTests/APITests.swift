//
//  APITests.swift
//  APITests
//
//  Created by Tóth Balázs on 01/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import XCTest
@testable import BlackSwanTest

class APITests: XCTestCase {
    let timeout: NSTimeInterval = 5
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - API Tests
    
    func testAPIConfiguredProperly() {
        let apiClient = RedditAPIClient()
        
        let baseURL = "https://www.reddit.com"
        let jsonEnding = ".json"
        
        XCTAssertEqual(apiClient.baseURL, baseURL, "BaseURL should be set correctly")
        XCTAssertEqual(apiClient.jsonEnding, jsonEnding, "URL Suffix should be set correctly")
    }
    
    func testAPIShouldLoadContentForSubreddit() {
        let apiClient = RedditAPIClient()
        let expectation = expectationWithDescription("Request")
        
        apiClient.requestSubRedditContent("/r/all") { (posts, error) in
            XCTAssertGreaterThan(posts.count, 0, "The client should return posts from /r/all")
            XCTAssertNil(error, "There should be no error present")
            
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
    }
    
    func testAPIShouldGiveErrorIfURLIsWrong() {
        let apiClient = RedditAPIClient()
        let expectation = expectationWithDescription("Request")
        
        apiClient.requestSubRedditContent("randomwords") { (posts, error) in
            XCTAssertTrue(posts.count == 0, "Wrong urls should not return posts")
            XCTAssertTrue(error != nil, "But should return an error")
            
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
    }
    
    func testAPIDoesNotContainNSFWPosts() {
        let apiClient = RedditAPIClient()
        let expectation = expectationWithDescription("Request")
        
        // The subreddit /r/nsfw only contains posts not suited for work, these should be left out
        apiClient.requestSubRedditContent("/r/nsfw") { (posts, error) in
            for post in posts {
                XCTAssertFalse(post.over_18)
            }
            
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(timeout, handler: nil)
    }
    
}
