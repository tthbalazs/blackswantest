//
//  SubRedditTests.swift
//  BlackSwanTest
//
//  Created by Tóth Balázs on 02/07/16.
//  Copyright © 2016 Tóth Balázs. All rights reserved.
//

import XCTest
@testable import BlackSwanTest

class SubRedditTests: XCTestCase {
    var testUser: User!
    
    override func setUp() {
        super.setUp()
        
        testUser = User()
        testUser.subReddits = []
        testUser.loadSubReddits()
    }
    
    override func tearDown() {
        super.tearDown()
        
        testUser.subReddits = []
        testUser.saveSubReddits()
    }
    
    // MARK: - SubReddit tests
    
    func testNoSubReddits() {
        XCTAssert(testUser.subReddits.count != 0, "Initially the user should have some subreddits")
    }
    
    func testSubRedditAddition() {
        let subRedditToAdd = SubReddit(name: "/r/testsubreddit")
        
        XCTAssertFalse(testUser.subReddits.contains { (subReddit) -> Bool in
            if let name = subReddit.name, toAddName = subRedditToAdd.name {
                return name == toAddName
            } else {
                return false
            }
        }, "Initially the subreddit is not present in the array")
        
        testUser.addSubReddit(subRedditToAdd.name!)
        
        XCTAssertTrue(testUser.subReddits.contains { (subReddit) -> Bool in
            if let name = subReddit.name, toAddName = subRedditToAdd.name {
                return name == toAddName
            } else {
                return false
            }
        }, "After adding the subreddit, it is present in the array")
    }
    
    func testSubRedditDeletion() {
        let name = "/r/toremove"
        let subRedditToRemove = SubReddit(name: name)
        
        testUser.addSubReddit(name)
        
        XCTAssertTrue(testUser.subReddits.contains { (subReddit) -> Bool in
            if let name = subReddit.name, toAddName = subRedditToRemove.name {
                return name == toAddName
            } else {
                return false
            }
            }, "Initially the subreddit is present in the array")
        
        testUser.removeSubReddit(name)
        
        XCTAssertFalse(testUser.subReddits.contains { (subReddit) -> Bool in
            if let name = subReddit.name, toAddName = subRedditToRemove.name {
                return name == toAddName
            } else {
                return false
            }
        }, "After removing the subreddit, it is not present in the array")
    }
}